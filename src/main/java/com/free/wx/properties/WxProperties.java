package com.free.wx.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 微信的配置
 */
@ConfigurationProperties(prefix = "wx" )
@Component
@Data
public class WxProperties {
    private String token;
    private String appid;//	小程序 appId
    private String secret;//		是	小程序 appSecret
    private String grantType;//		是	授权类型，此处只需填写 authorization_code

}
