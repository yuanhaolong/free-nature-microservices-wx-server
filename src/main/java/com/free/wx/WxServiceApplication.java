package com.free.wx;

import com.free.common.security.annotation.EnableCustomConfig;
import com.free.common.security.annotation.EnableRyFeignClients;
import com.free.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 微信模块
 *
 * @author free
 */
@EnableCustomSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class},scanBasePackages = {"com.free.**"})
@EnableCustomConfig
@EnableRyFeignClients
//@SpringBootApplication
public class WxServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxServiceApplication.class, args);
        System.out.println("微信模块启动完成" );
    }
}
