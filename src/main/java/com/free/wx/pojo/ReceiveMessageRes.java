package com.free.wx.pojo;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "xml" )
@XmlAccessorType(XmlAccessType.FIELD)
public class ReceiveMessageRes {
    String ToUserName;//是	接收方账号（收到的OpenID）
    String FromUserName;//是	开发者微信号
    String CreateTime;//是	消息创建时间 （整型）
    String MsgType;//是	消息类型，文本为text
    String Content;//是	回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
}
