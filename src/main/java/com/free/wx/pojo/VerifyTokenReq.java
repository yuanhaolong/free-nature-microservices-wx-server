package com.free.wx.pojo;

import lombok.Data;

@Data
public class VerifyTokenReq {
    String signature;//微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
    String timestamp;//时间戳
    String nonce;//随机数
    String echostr;//随机字符串
}
