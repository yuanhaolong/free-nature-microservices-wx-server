package com.free.wx.pojo;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "xml" )
@XmlAccessorType(XmlAccessType.FIELD)
public class ReceiveMessageReq {
    String ToUserName;//	开发者微信号
    String FromUserName;//	发送方账号（一个OpenID）
    String CreateTime;//消息创建时间 （整型）
    String MsgType;//	消息类型，文本为text
    String Content;//文本消息内容
    String MsgId;//消息id，64位整型
    String MsgDataId;//消息的数据ID（消息如果来自文章时才有）
    String Idx;//多图文时第几篇文章，从1开始（消息如果来自文章时才有）
}
