FROM alibabadragonwell/dragonwell:21-anolis

WORKDIR /

ARG JAR_FILE=target/free-wx-service.jar

COPY ${JAR_FILE} app.jar

EXPOSE 4000

ENV TZ=Asia/Shanghai JAVA_OPTS="-Xms128m -Xmx256m -Djava.security.egd=file:/dev/./urandom"

CMD sleep 1; java $JAVA_OPTS -jar app.jar --server.ip=$HOST --server.port=$PORT --spring.profiles.active=${ACTIVE:local}